// console.log("Hello World");

// exponent operator

const firstNum = 8 ** 2;
console.log(firstNum);

// after ES6 Update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

// template literals

let name = "John";

// pre-template literal strings
let message = 'Hello ' + name + '! Welome to programming';
console.log("Message without template literals: " + message);

// using template literal
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// Multi-line using template literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.
`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// array destructuring

const fullName = ["Juan", "Dela", "Cruz"];

// pre array destructureing
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you`);

// array destructuring
const [firstName, middleName, lastName] = fullName;

// using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// pre object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! Its good to see you!`)

// oobject destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`);

// using destructured variable to a function
function getFullName( {givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Function
const hello = () => {
	console.log("Hello World");
}
hello();

// traditional function
/*function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("John", "Doe", "Smith");
*/
// arrow function
const printFullName = (firstName, middleName, lastName) =>{
	console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("John", "Doe", "Smith");

const students = ["John", "Jane", "Judy"];

// arrow function with loops

// pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student`);
});

// arrow function
students.forEach((student) => {
	console.log(`${student} is a student`);
});

// default function argument value 

const greet = (name = "User") => {
	return `Good Morning, ${name}`
}

console.log(greet());
console.log(greet("Erica"));

// class-based object blueprints

// creating a class

class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year

	}
}
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", "2021");
console.log(myNewCar);